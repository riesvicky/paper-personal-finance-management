# PAPER.ID TEST CASE - PersonalFinanceManager

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 9.1.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Background

We want to create an app that helps user to track their daily / monthly expenses and incomes,
thus ultimately helping them not to overspend or lose track of their finances.
At the same time, the app need to be able inform the users what are the highest expense, whats
left in a month, so that they can improve their expense and income pattern in the future.

## Requirement

The app needs to display a dashboard of some sorts with information about total expenses per
months, among others
User should be able to input Income (Salary, etc) or expense (electric bills etc) with descriptions,
names, dollar / rupiah amounts
It is even better if a user can input multiple entries (i.e. 2 or 3 incomes) at the same time or flow
User needs to see the filterable and searchable list of the incomes and expenses entries. Then
they can do Update, View, and Delete Action on them

## Scope & What we look for

As all of this is purely Front End, front end state needs to be managed and used as a BE
replacement of some sort.
Structure, Code Architecture, and good usage of design and implementation pattern are the
core of this project case
Feel free to use your delightful design sense to design the UI, it is not important to create the
most beautiful design, but the logical structure of the UI pattern matters
You are free to use any front end stacks & library that you are familiar with