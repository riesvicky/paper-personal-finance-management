import { Injectable } from '@angular/core';
import { Setting } from '../models/setting.model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SettingService {
  constructor() { }

  storeSetting(dataset: any = null): Observable<string> {
    console.log(dataset)

    if(dataset === null)
      dataset = this.default();

    try{
      let request: Observable<string> = Observable.create(observer => {
        localStorage.setItem('SETTING_PREFERENCE', JSON.stringify(dataset));
  
        setTimeout(() => observer.complete(), 3000);
      })
  
      return request;
    } catch (error) {
      throw new Error(error)
    }

  }

  check() {
    const obj = localStorage.getItem('SETTING_PREFERENCE');
    
    return obj != undefined ? true : false;
  }

  getSetting() {
    return JSON.parse(localStorage.getItem('SETTING_PREFERENCE'));
  }

  getTargetByMonth(month: string) {
    return JSON.parse(localStorage.getItem('SETTING_PREFERENCE'))[month];
  }

  default(): Setting {
    return {
      january   : 0,
      february  : 0,
      march     : 0,
      april     : 0,
      may       : 0,
      june      : 0,
      july      : 0,
      august    : 0,
      september : 0,
      october   : 0,
      november  : 0,
      december  : 0  
    }
  }
}
