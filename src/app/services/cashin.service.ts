import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { FormPref } from '../components/modals/finance-cashin/finance-cashin.component';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class CashinService {
  
  constructor(private helperService: HelperService) { }

  getIncomes(filterObj: any): Observable<any> {
    let incomes: any;
    const dataset = JSON.parse(localStorage.getItem('APP_IN'));
    let request = Observable.create(observer => {
      if(filterObj.search == ''){
        if(localStorage.getItem('APP_IN') != null)
          incomes = dataset;
        else
          incomes = []
      } else {
        if(localStorage.getItem('APP_IN') != null)
          incomes = this.helperService.filterByValue(dataset, filterObj.search)
        else
          incomes = []
      }

      setTimeout(() => {
        observer.next(incomes)
        observer.complete()
      }, 3000)
    })

    return request;
  }

  getIncomesByDate(filterObj: any, datasetParam = []) {
    let incomes: any;
    const dataset = datasetParam.length == 0 ? JSON.parse(localStorage.getItem('APP_IN')) : datasetParam;
    console.log(dataset)
    let request = Observable.create(observer => {
      if(localStorage.getItem('APP_IN') != null)
        incomes = this.helperService.filterByDate(dataset, filterObj)
      else
        incomes = [] 
      
      setTimeout(() => {
        observer.next(incomes)
        observer.complete()
      }, 3000)
    })

    return request;
  }

  getTotalIncomeByMonth( 
    month: number = new Date().getMonth(),
    year: number = new Date().getFullYear()
  ) {
    let filterObj = {
      search: '',
      startDate: this.helperService.getFirstDate(year, month),
      endDate: this.helperService.getLastDate(year, month)
    },
    total: number = 0;

    let request = Observable.create(observer => {
      let request$ = this.getIncomesByDate(filterObj, []).subscribe({
        next: request =>{
          request.map(income => {
            total += income.incomeValue
          })

          setTimeout(() => observer.next(total), 500)
        }
      })
    })

  return request;
  }

  getHighestIncomeByMonth(
    month: number = new Date().getMonth(),
    year: number = new Date().getFullYear()
  ) {
    let filterObj = {
      search: '',
      startDate: this.helperService.getFirstDate(year, month),
      endDate: this.helperService.getLastDate(year, month)
    },
    maxIncome: number = 0;

    let request = Observable.create(observer => {
      let request$ = this.getIncomesByDate(filterObj, []).subscribe({
        next: request =>{
          if(request.length > 0)
            maxIncome = Math.max.apply(Math, request.map(function(income) { return income.incomeValue; }))
          else
            maxIncome = 0
            
          console.log(request, maxIncome)
          setTimeout(() => observer.next(maxIncome), 500)
        }
      })
    })

    return request;
  }

  getTheLastId(): number {
    return parseInt(localStorage.getItem('IN_MEMORY_INCOME_LAST_ID'))
  }
  
  setTheLastId(id: number) {
    localStorage.setItem('IN_MEMORY_INCOME_LAST_ID', id.toString());
  }

  setIncome(dataset: any): Observable<string> {
    const S_MESSAGE = 'Set Income Done'
    let id;

    try{
      let request = Observable.create(observer => {
        let appIn = JSON.parse(localStorage.getItem('APP_IN'));
        if(appIn == undefined)
          appIn = []

        if(appIn.length > 0){ 
          id = this.getTheLastId();
          id++;
          dataset['id'] = id;
          this.setTheLastId(id);

          appIn.push(dataset)
          localStorage.setItem('APP_IN', JSON.stringify(appIn)); 
        }
        else {
          id = 1;
          this.setTheLastId(id);
          dataset['id'] = id;
          localStorage.setItem('APP_IN', JSON.stringify([dataset]));
        }
        
        setTimeout(() => observer.complete(S_MESSAGE), 3000)
      })

      return request;
    } catch(error) {
      throw new Error(error);
    }

    return;
  }

  setIncomes(datasets: any): Observable<string> {
    // Request observable 
    let request = Observable.create(observer => {
      let i = 1;
      datasets.map(dataset => {        
        dataset.value.dpReformatted = this.helperService.dateReformat(
                                        dataset.value.dp
                                      );

        this.setIncome(dataset.value).subscribe({
          error: err => {
            console.error(`Field ${i} was error: Error message ${ err }`)
          },
          complete: () => {
            console.log(`Field ${i} was completed`)
            
            i++;
          }
        })

      })

      // In order to simulate mock API. 3 seconds delay
      setTimeout(() => observer.complete(), 3000);
    })

    return request;
  }

  updateIncome(dataset: FormPref) {
    const S_MESSAGE = 'Update Income Done'

    let request = Observable.create(observer => {
      let appIn = JSON.parse(localStorage.getItem('APP_IN'));

      let deleted = appIn.filter(x => x.id !== dataset.id);
      deleted.push(dataset);

      localStorage.setItem('APP_IN', JSON.stringify(deleted));
      
      setTimeout(() => observer.complete(S_MESSAGE), 3000)
    })

    return request;
  }

  deleteById(dataset: FormPref) {
    const S_MESSAGE = 'Delete Income Done'

    let request = Observable.create(observer => {
      let appIn = JSON.parse(localStorage.getItem('APP_IN'));
      let deleted = appIn.filter(x => x.id !== dataset.id);
      console.log(deleted, appIn)

      localStorage.setItem('APP_IN', JSON.stringify(deleted));
      
      setTimeout(() => { 
        observer.next(JSON.parse(localStorage.getItem('APP_IN')))
        observer.complete(S_MESSAGE)
      }, 3000)
    })

    return request;
  }
}
