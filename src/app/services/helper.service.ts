import { Injectable } from '@angular/core';
import { filter, first } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HelperService {
  private date = new Date();
  constructor() { }

  getFirstDate(year = null,  month = null) {
    let date = new Date(
      year == null ? this.date.getFullYear() : year, 
      month == null ? (this.date.getMonth() + 1) : month, 
      1
    )
    console.error(date.getMonth(), month)
    return {
      year: date.getFullYear(),
      month: date.getMonth(),
      day: date.getDate()
    }
  }

  getToday() {
    let date = new Date()
    return {
      year: date.getFullYear(),
      month: (date.getMonth() + 1),
      day: date.getDate()
    }
  }

  getLastDate(year = null,  month = null) {
    let date = new Date(
      year == null ? this.date.getFullYear() : year, 
      month == null ? this.date.getMonth() + 1 : month, 
      0
    );
    return {
      year: date.getFullYear(),
      month: (date.getMonth() + 1),
      day: date.getDate()
    }
  }

  getYear() {
    return new Date().getFullYear()
  }

  filterByValue(array, string) {
    return array.filter(o =>
        Object.keys(o).some(k => {
          if(typeof(o[k]) == 'string')
            return o[k].toLowerCase().includes(string.toLowerCase())
          else if(typeof(o[k]) == 'number'){
            const val = o[k].toString();
            return val.toLowerCase().includes(string.toLowerCase())
          }
        }));
  }

  filterByDate(dataset, filterObj) {
    return dataset.filter(a => {
        let aDate = new Date(a.dpReformatted),
        firstDate = new Date(this.dateReformat(filterObj.startDate)),
        endDate   = new Date(this.dateReformat(filterObj.endDate))

        return aDate >= firstDate && aDate <= endDate;
    });
  }

  deviation(expenseValue: number, incomeValue: number){
    return incomeValue - expenseValue;
  }

  dateReformat(date: any) {
    return `${date.month}/${date.day}/${date.year}`;
  }
}
