import { Injectable } from '@angular/core';
import { Observable } from 'rxjs'
import { FormPref } from '../components/modals/finance-expense/finance-expense.component';
import { HelperService } from './helper.service';

@Injectable({
  providedIn: 'root'
})
export class ExpenseService {
  
  constructor(private helperService: HelperService) { }

  getExpenses(filterObj: any): Observable<any> {
    let expenses: any;
    const dataset = JSON.parse(localStorage.getItem('APP_EX'));
    let request = Observable.create(observer => {
      if(filterObj.search == ''){
        if(localStorage.getItem('APP_EX') != null)
          expenses = dataset;
        else
          expenses = []
      } else {
        if(localStorage.getItem('APP_EX') != null)
          expenses = this.helperService.filterByValue(dataset, filterObj.search)
        else
          expenses = []
      }

      setTimeout(() => {
        observer.next(expenses)
        observer.complete()
      }, 3000)
    })

    return request;
  }

  getExpensesByDate(filterObj: any, datasetParam = []) {
    let expenses: any;
    const dataset = datasetParam.length == 0 ? JSON.parse(localStorage.getItem('APP_EX')) : datasetParam;
    
    let request = Observable.create(observer => {
      if(localStorage.getItem('APP_EX') != null){
        console.error(localStorage.getItem('APP_EX') != null)
        expenses = this.helperService.filterByDate(dataset, filterObj)}
      else
        expenses = []
      
      setTimeout(() => {
        observer.next(expenses)
        observer.complete()
      }, 3000)
    })

    return request;
  }

  getHighestExpenseByMonth(
    month: number = new Date().getMonth(),
    year: number = new Date().getFullYear()
  ) {
    let filterObj = {
      search: '',
      startDate: this.helperService.getFirstDate(year, month),
      endDate: this.helperService.getLastDate(year, month)
    },
    maxExpense: number = 0;

    let request = Observable.create(observer => {
      let request$ = this.getExpensesByDate(filterObj, []).subscribe({
        next: request =>{
          if(request.length > 0)
            maxExpense = Math.max.apply(Math, request.map(function(expense) { return expense.expenseValue; }))
          else
            maxExpense = 0

          console.log(request, maxExpense)
          setTimeout(() => observer.next(maxExpense), 500)
        }
      })
    })

    return request;
  }

  getTotalExpenseByMonth( 
                          month: number = new Date().getMonth(),
                          year: number = new Date().getFullYear()
                        ) {
    let filterObj = {
      search: '',
      startDate: this.helperService.getFirstDate(year, month),
      endDate: this.helperService.getLastDate(year, month)
    },
    total: number = 0;

    let request = Observable.create(observer => {
      let request$ = this.getExpensesByDate(filterObj, []).subscribe({
        next: request =>{
          request.map(expense => {
            total += expense.expenseValue
          })
  
          setTimeout(() => observer.next(total), 500)
        }
      })
    })
    
    return request;
  }

  getTheLastId(): number {
    return parseInt(localStorage.getItem('IN_MEMORY_EXPENSE_LAST_ID'))
  }
  
  setTheLastId(id: number) {
    localStorage.setItem('IN_MEMORY_EXPENSE_LAST_ID', id.toString());
  }

  setExpense(dataset: any): Observable<string> {
    const S_MESSAGE = 'Set Expense Done'
    let id;

    try{
      let request = Observable.create(observer => {
        let appEx = JSON.parse(localStorage.getItem('APP_EX'));
        if(appEx == undefined)
          appEx = []
  
        if(appEx.length > 0){ 
          id = this.getTheLastId();
          id++;
          dataset['id'] = id;
          this.setTheLastId(id);
  
          appEx.push(dataset)
          localStorage.setItem('APP_EX', JSON.stringify(appEx)); 
        }
        else {
          id = 1;
          this.setTheLastId(id);
          dataset['id'] = id;
          localStorage.setItem('APP_EX', JSON.stringify([dataset]));
        }
        
        setTimeout(() => observer.complete(S_MESSAGE), 3000)
      })

      return request;
    } catch(error) {
      throw new Error (error)
    }

    return;
  }

  setExpenses(datasets: any): Observable<string> {
    // Request observable 
    let request = Observable.create(observer => {
      let i = 1;
      datasets.map(dataset => {        
        dataset.value.dpReformatted = this.helperService.dateReformat(
                                        dataset.value.dp
                                      );

        this.setExpense(dataset.value).subscribe({
          error: err => {
            console.error(`Field ${i} was error: Error message ${ err }`)
          },
          complete: () => {
            console.log(`Field ${i} was completed`)

            i++;
          }
        })
      })

      // In order to simulate mock API. 3 seconds delay
      setTimeout(() => observer.complete(), 3000);
    })

    return request;
  }

  updateExpense(dataset: FormPref) {
    const S_MESSAGE = 'Update Expense Done'

    let request = Observable.create(observer => {
      let appEx = JSON.parse(localStorage.getItem('APP_EX'));

      let deleted = appEx.filter(x => x.id !== dataset.id);
      deleted.push(dataset);

      localStorage.setItem('APP_EX', JSON.stringify(deleted));
      
      setTimeout(() => observer.complete(S_MESSAGE), 3000)
    })

    return request;
  }

  deleteById(dataset: FormPref) {
    const S_MESSAGE = 'Update Expense Done'

    let request = Observable.create(observer => {
      let appEx = JSON.parse(localStorage.getItem('APP_EX'));
      let deleted = appEx.filter(x => x.id !== dataset.id);

      localStorage.setItem('APP_EX', JSON.stringify(deleted));
      
      setTimeout(() => { 
        observer.next(JSON.parse(localStorage.getItem('APP_EX')))
        observer.complete(S_MESSAGE)
      }, 3000)
    })

    return request;
  }
}
