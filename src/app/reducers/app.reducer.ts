// Section 1
import { Action } from '@ngrx/store'
import { App } from './../models/app.model'
import * as AppActions from './../actions/app.action';

// Section 2
const initialState: App = {
  title: 'Home'
}

// Section 3
export function reducer(state: App = initialState, action: AppActions.Actions) {
  switch(action.type) {
    case AppActions.SET_TITLE:
      return Object.assign({}, state, { title: action.payload });
    default:
      return state;
  }
}