import { Injectable } from '@angular/core'
import { Action } from '@ngrx/store'
import { App } from '../models/app.model'

export const SET_TITLE       = '[TITLE] Set';

export class SetTitle implements Action {
  readonly type = SET_TITLE
  constructor(public payload: string) {}
}

export type Actions = SetTitle