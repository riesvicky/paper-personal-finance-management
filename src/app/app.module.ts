import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { StoreModule } from '@ngrx/store';
import { reducer } from './reducers/app.reducer';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './components/navigation/navigation.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { NgSelect2Module } from 'ng-select2';
import { HomeComponent } from './pages/home/home.component';
import { CashinComponent } from './pages/cashin/cashin.component';
import { ExpenseComponent } from './pages/expense/expense.component';
import { LazyLoaderComponent } from './components/lazy-loader/lazy-loader.component';
import { SearchDoodleComponent } from './components/search-doodle/search-doodle.component';
import { FinanceExpenseComponent } from './components/modals/finance-expense/finance-expense.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CurrencyMaskModule } from "ng2-currency-mask";
import { DataTablesModule } from 'angular-datatables';
import { FinanceCashinComponent } from './components/modals/finance-cashin/finance-cashin.component';
import { CreateMultipleExpensesComponent } from './pages/create-multiple-expenses/create-multiple-expenses.component';
import { CreateMultipleIncomesComponent } from './pages/create-multiple-incomes/create-multiple-incomes.component';
import { SettingComponent } from './pages/setting/setting.component';
import { KnowledgeBaseComponent } from './pages/knowledge-base/knowledge-bae.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NavbarComponent,
    HomeComponent, 
    ExpenseComponent, 
    CashinComponent, 
    LazyLoaderComponent,
    SearchDoodleComponent, 
    FinanceExpenseComponent, FinanceCashinComponent, CreateMultipleExpensesComponent, CreateMultipleIncomesComponent, SettingComponent, KnowledgeBaseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule,
    FormsModule,
    NgSelect2Module,
    ReactiveFormsModule,
    NgbModule,
    CurrencyMaskModule,
    DataTablesModule,
    StoreModule.forRoot({
      metareducer: reducer
    }),
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
