import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/home/home.component';
import { ExpenseComponent } from './pages/expense/expense.component';
import { CashinComponent } from './pages/cashin/cashin.component';
import { CreateMultipleExpensesComponent } from './pages/create-multiple-expenses/create-multiple-expenses.component';
import { CreateMultipleIncomesComponent } from './pages/create-multiple-incomes/create-multiple-incomes.component';
import { SettingComponent } from './pages/setting/setting.component';
import { KnowledgeBaseComponent } from './pages/knowledge-base/knowledge-bae.component';


const routes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'expenses', component: ExpenseComponent },
  { path: 'incomes', component: CashinComponent },
  { path: 'expenses/create-multiple', component: CreateMultipleExpensesComponent },
  { path: 'incomes/create-multiple', component: CreateMultipleIncomesComponent },
  { path: 'setting', component: SettingComponent },
  { path: 'knowledge-base', component: KnowledgeBaseComponent }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
