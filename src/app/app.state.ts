import { App } from './models/app.model';

export interface AppState {
  readonly metareducer: App;
}