import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-search-doodle',
  templateUrl: './search-doodle.component.html',
  styleUrls: ['./search-doodle.component.scss']
})
export class SearchDoodleComponent implements OnInit {
  @Input() public headImage;
  @Input() public description;
  @Input() public headImageClass;

  constructor() { }

  ngOnInit(): void {
  }

}
