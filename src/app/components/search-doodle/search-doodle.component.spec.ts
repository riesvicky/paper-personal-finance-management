import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchDoodleComponent } from './search-doodle.component';

describe('SearchDoodleComponent', () => {
  let component: SearchDoodleComponent;
  let fixture: ComponentFixture<SearchDoodleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SearchDoodleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchDoodleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
