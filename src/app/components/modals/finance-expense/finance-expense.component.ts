import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { ExpenseService } from 'src/app/services/expense.service';
import { Subscription } from 'rxjs';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-finance-expense',
  templateUrl: './finance-expense.component.html',
  styleUrls: ['./finance-expense.component.scss']
})
export class FinanceExpenseComponent implements OnInit {
  @ViewChild('content', {static: true}) content: ElementRef;
  @Output() public saveEvent = new EventEmitter();

  public closeResult = '';
  public control: FormControl = new FormControl('dp', Validators.minLength(4));
  public formState = '';
  public formPref: FormPref = {
    dp: new Date(),
    dpReformatted: '',
    expenseValue: '',
    expenseDescription: '',
    id: 0
  }

  constructor(
    private modalService: NgbModal, 
    private expenseService: ExpenseService,
    private helperService: HelperService
  ) { }

  ngOnInit(): void {
  }

  setDefaultFormPref() {
    this.formPref = {
      dp: new Date(),
      dpReformatted: '',
      expenseValue: '',
      expenseDescription: '',
      id: 0
    };
  }

  storeExpense() {
    this.formPref.dpReformatted = this.helperService.dateReformat(this.formPref.dp);    
    const form: JQuery<HTMLElement> = $('form#expenseForm');

    form.addClass('loading');

    if(this.formState == 'create') {
      let setExpense: Subscription = this.expenseService.setExpense(this.formPref).subscribe({
        complete: () => {
          form.removeClass('loading')
          this.modalService.dismissAll();
          
          this.saveEvent.emit({
            alert: {
              success: true,
              successMessage: 'Expense has been created',
              error: false,
              errorMessage: ''
            },
            dataset: JSON.parse(localStorage.getItem('APP_EX'))
          });
          this.setDefaultFormPref();

          setExpense.unsubscribe()
        }
      })
    } else if (this.formState == 'edit') {
      let updateExpense: Subscription = this.expenseService.updateExpense(this.formPref).subscribe({
        complete: () => {
          form.removeClass('loading')
          this.modalService.dismissAll();
          
          this.saveEvent.emit({
            alert: {
              success: true,
              successMessage: 'Expense has been updated',
              error: false,
              errorMessage: ''
            },
            dataset: JSON.parse(localStorage.getItem('APP_EX'))
          });
          this.setDefaultFormPref();

          updateExpense.unsubscribe()
        }
      })
    }
  }

  onOpenModal(): void {
    this.setDefaultFormPref();
    this.formState = 'create';

    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static'}).result.then(() => {
      this.formState = '';
    }, () => {
      this.formState = '';
    });
  }

  onOpenEditModal(dataset: FormPref){
    this.formState = 'edit';
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static'}).result.then(() => {
      this.formState = '';
    }, () => {
      this.formState = '';
    });;

    if(Object.keys(dataset).length > 0 && dataset.constructor === Object){
      console.log(dataset);
      this.formPref = dataset;
    }
  }
}

export interface FormPref {
  dp: any,
  dpReformatted: string,
  expenseValue: string,
  expenseDescription: string,
  id: number
}
