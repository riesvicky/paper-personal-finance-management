import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FinanceCashinComponent } from './finance-cashin.component';

describe('FinanceCashinComponent', () => {
  let component: FinanceCashinComponent;
  let fixture: ComponentFixture<FinanceCashinComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FinanceCashinComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FinanceCashinComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
