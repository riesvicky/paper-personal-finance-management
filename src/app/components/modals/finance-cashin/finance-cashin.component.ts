import { Component, OnInit, ViewChild, ElementRef, Output, EventEmitter } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, Validators } from '@angular/forms';
import * as $ from 'jquery';
import { CashinService } from 'src/app/services/cashin.service';
import { Subscription } from 'rxjs';
import { HelperService } from 'src/app/services/helper.service';

@Component({
  selector: 'app-finance-cashin',
  templateUrl: './finance-cashin.component.html',
  styleUrls: ['./finance-cashin.component.scss']
})
export class FinanceCashinComponent implements OnInit {
  @ViewChild('content', {static: true}) content: ElementRef;
  @Output() public saveEvent = new EventEmitter();

  public closeResult = '';
  public control: FormControl = new FormControl('dp', Validators.minLength(4));
  public formState = '';
  public formPref: FormPref = {
    dp: new Date(),
    dpReformatted: '',
    incomeValue: '',
    incomeDescription: '',
    id: 0
  }

  constructor(private modalService: NgbModal, private cashinService: CashinService, private helperService: HelperService) { }

  ngOnInit(): void {
  }

  setDefaultFormPref() {
    this.formPref = {
      dp: new Date(),
      dpReformatted: '',
      incomeValue: '',
      incomeDescription: '',
      id: 0
    };
  }

  storeIncome() {
    this.formPref.dpReformatted = this.helperService.dateReformat(this.formPref.dp)    
    const form: JQuery<HTMLElement> = $('form#incomeForm');

    form.addClass('loading');

    if(this.formState == 'create') {
      let setIncome: Subscription = this.cashinService.setIncome(this.formPref).subscribe({
        complete: () => {
          form.removeClass('loading')
          this.modalService.dismissAll();
          
          this.saveEvent.emit({
            alert: {
              success: true,
              successMessage: 'Income has been created',
              error: false,
              errorMessage: ''
            },
            dataset: JSON.parse(localStorage.getItem('APP_IN'))
          });
          this.setDefaultFormPref();

          setIncome.unsubscribe()
        }
      })
    } else if (this.formState == 'edit') {
      let updateIncome: Subscription = this.cashinService.updateIncome(this.formPref).subscribe({
        complete: () => {
          form.removeClass('loading')
          this.modalService.dismissAll();
          
          this.saveEvent.emit({
            alert: {
              success: true,
              successMessage: 'Income has been updated',
              error: false,
              errorMessage: ''
            },
            dataset: JSON.parse(localStorage.getItem('APP_IN'))
          });
          this.setDefaultFormPref();

          updateIncome.unsubscribe()
        }
      })
    }
  }

  onOpenModal(): void {
    this.setDefaultFormPref();
    this.formState = 'create';

    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static'}).result.then(() => {
      this.formState = '';
    }, () => {
      this.formState = '';
    });
  }

  onOpenEditModal(dataset: FormPref){
    this.formState = 'edit';
    this.modalService.open(this.content, {ariaLabelledBy: 'modal-basic-title', backdrop: 'static'}).result.then(() => {
      this.formState = '';
    }, () => {
      this.formState = '';
    });;

    if(Object.keys(dataset).length > 0 && dataset.constructor === Object){
      console.log(dataset);
      this.formPref = dataset;
    }
  }
}

export interface FormPref {
  dp: any,
  dpReformatted: string,
  incomeValue: string,
  incomeDescription: string,
  id: number
}
