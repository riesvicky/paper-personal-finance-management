import { Component } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from "./app.state";
import { App } from './models/app.model';
import * as $ from 'jquery';
import * as AppActions from "./actions/app.action";
import { SettingService } from './services/setting.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  // section 1
  title: string = null;
  metareducer$: Observable<App>;
  //section 2
  constructor(private store: Store<AppState>, private settingService: SettingService) {
    this.metareducer$ = store.select('metareducer');
    this.metareducer$.subscribe(metareducer => {
      this.title = metareducer.title;
    });
  }

  ngOnInit() {
    let navbar = $('#navbarSupportedContent');
    
    console.log(`${navbar.closest('#navbar-wrapper').outerWidth()}px`)
    $(window).bind('scroll', function () {
      if ($(window).scrollTop() > 50) {
        if(!navbar.hasClass('fixed'))
          navbar.addClass('fixed');
        $('#navbarSupportedContent.fixed').css({
          width: `${navbar.closest('#navbar-wrapper').outerWidth()}px`
        })
      } else {
        if(navbar.hasClass('fixed'))
          navbar.removeClass('fixed');
      }
    });
    
    if(!this.settingService.check())
      this.settingService.storeSetting().subscribe()
  }
}
