export interface Alert {
  success: boolean,
  successMessage: string,
  error: boolean,
  errorMessage: string
}