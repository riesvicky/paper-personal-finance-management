import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HelperService } from '../../services/helper.service';
import { SettingService } from '../../services/setting.service';
import { Setting } from '../../models/setting.model';
import * as $ from 'jquery';
import { Alert } from '../../models/alert.model';
import { CONFIG } from 'src/app/constants/config';

@Component({
  selector: 'app-setting',
  templateUrl: './setting.component.html',
  styleUrls: ['./setting.component.scss']
})
export class SettingComponent implements OnInit {
  public settingPref: Setting = this.settingService.getSetting();
  public alert = this.defaultAlertState();
  public CONFIG = CONFIG;
  public months: FormGroup = this.fb.group({
    january   : [
                  this.settingPref.january == 0 ? 0 : this.settingPref.january,
                  [Validators.required]
                ],
    february  : [
                  this.settingPref.february == 0 ? 0 : this.settingPref.february,
                  [Validators.required]
                ],
    march     : [
                  this.settingPref.march == 0 ? 0 : this.settingPref.march,
                  [Validators.required]
                ],
    april     : [
                  this.settingPref.april == 0 ? 0 : this.settingPref.april,
                  [Validators.required]
                ],
    may       : [
                  this.settingPref.may == 0 ? 0 : this.settingPref.may,
                  [Validators.required]
                ],
    june      : [
                  this.settingPref.june == 0 ? 0 : this.settingPref.june,
                  [Validators.required]
                ],
    july      : [
                  this.settingPref.july == 0 ? 0 : this.settingPref.july,
                  [Validators.required]
                ],
    august    : [
                  this.settingPref.august == 0 ? 0 : this.settingPref.august,
                  [Validators.required]
                ],
    september : [
                  this.settingPref.september == 0 ? 0 : this.settingPref.september,
                  [Validators.required]
                ],
    october   : [
                  this.settingPref.october == 0 ? 0 : this.settingPref.october,
                  [Validators.required]
                ],
    november  : [
                  this.settingPref.november == 0 ? 0 : this.settingPref.november,
                  [Validators.required]
                ],
    december  : [
                  this.settingPref.december == 0 ? 0 : this.settingPref.december,
                  [Validators.required]
                ]  
  })
  public year: number = this.helperService.getYear();

  constructor(
    private fb: FormBuilder, 
    private helperService: HelperService,
    private settingService: SettingService
  ) {
    console.log(this.months.value)
  }

  ngOnInit(): void {
  }

  storeSetting() {
    let form: JQuery<HTMLElement> = $('form');
    
    if(!form.hasClass('loading'))
      form.addClass('loading');
    
    this.defaultAlertState();

    this.settingService.storeSetting(this.months.value).subscribe({
      error: err => {
        console.error(`It has certain errors while trying to store setting preference. Error message : ${err}`);

        setTimeout(() => {
          if(form.hasClass('loading'))
            form.removeClass('loading');
            
          this.alert.error = true;
          window.scrollTo(0, 0)
        }, 3000)
      },
      complete: () => {
        console.log('Store Setting Complete!');

          setTimeout(() => {
            if(form.hasClass('loading'))
              form.removeClass('loading');

            this.alert.success = true;
            window.scrollTo(0, 0)
          }, 3000)
      } 
    });
  }

  defaultAlertState(): Alert {
    return {
      success: false,
      successMessage: '',
      error: false,
      errorMessage: ''
    }
  }

}
