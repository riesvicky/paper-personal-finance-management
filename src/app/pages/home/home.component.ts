import { Component, OnInit } from '@angular/core';
import { Store } from "@ngrx/store";
import { App } from "../../models/app.model";
import { AppState } from "../../app.state";
import * as AppActions from "../../actions/app.action";
import { CONFIG } from '../../constants/config';
import { HelperService } from '../../services/helper.service';
import { ExpenseService } from '../../services/expense.service';
import { CashinService } from '../../services/cashin.service';
import { SettingService } from '../../services/setting.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public _initAnimation = true;
  public APP_URL = CONFIG.APP_URL;
  public MONTHS = CONFIG.MONTHS;
  public totalExpenses = [];
  public totalIncomes = [];
  public highestExpenses = [];
  public highestIncomes = [];
  public expensesLimitTarget = [];
  public images = {
    accountingModal     : `${this.APP_URL}/assets/images/accounting/passiva.svg`,
    iconPelajari        : `${this.APP_URL}/assets/images/main-dashboard/Icon-Pelajari.svg`,
    costPlane           : `${this.APP_URL}/assets/images/accounting/cost-plane.svg`,
    incomePlane         : `${this.APP_URL}/assets/images/accounting/income-plane.svg`,
    mobileSalesOrder    : `${this.APP_URL}/assets/images/main-dashboard/mobile-sales-order.svg`,
    mobileSalesInvoice  : `${this.APP_URL}/assets/images/main-dashboard/mobile-sales-invoice.svg`,
    mobilePaymentIn     : `${this.APP_URL}/assets/images/main-dashboard/mobile-payment-in.svg`,
    pohonDanHangar      : `${this.APP_URL}/assets/images/main-dashboard/Pohon-dan-Hangar.svg`
  }
  

  constructor(
    private store: Store<AppState>,
    private helperService: HelperService,
    private expenseService: ExpenseService,
    private cashinService: CashinService,
    private settingService: SettingService
  ) {
    this.store.dispatch(new AppActions.SetTitle('HOME'))
  }

  ngOnInit(): void {
    console.log(this._initAnimation)
    let settingPref = this.settingService.getSetting();

    this.MONTHS.map(month => {
      this.expenseService.getTotalExpenseByMonth(month.index).subscribe({
        next: total => {
          this.totalExpenses[(month.index - 1)] = total;
        }
      })

      this.cashinService.getTotalIncomeByMonth(month.index).subscribe({
        next: total => {
          this.totalIncomes[(month.index - 1)] = total;
        }
      })

      this.expenseService.getHighestExpenseByMonth(month.index).subscribe({
        next: expense => {
          this.highestExpenses[(month.index - 1)] = expense;
        }
      })

      this.cashinService.getHighestIncomeByMonth(month.index).subscribe({
        next: income => {
          this.highestIncomes[(month.index - 1)] = income;
        }
      })

      this.expensesLimitTarget[(month.name)] = settingPref[(month.name)]
    })
    
  }

}
