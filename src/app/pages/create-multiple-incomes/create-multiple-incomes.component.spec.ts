import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMultipleIncomesComponent } from './create-multiple-incomes.component';

describe('CreateMultipleIncomesComponent', () => {
  let component: CreateMultipleIncomesComponent;
  let fixture: ComponentFixture<CreateMultipleIncomesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMultipleIncomesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMultipleIncomesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
