import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl, FormArray, NgForm } from '@angular/forms'
import { HelperService } from '../../services/helper.service';
import { Router } from '@angular/router';
import { CashinService } from '../../services/cashin.service';
import { CONFIG } from 'src/app/constants/config';

@Component({
  selector: 'app-create-multiple-incomes',
  templateUrl: './create-multiple-incomes.component.html',
  styleUrls: ['./create-multiple-incomes.component.scss']
})
export class CreateMultipleIncomesComponent implements OnInit {
  public CONFIG = CONFIG;
  // Instanciate FormGroup with JSON that represent with fb.group <FormBuilder>
  // That has a datasets array
  public formPrefs: FormGroup = this.fb.group({
    datasets : this.fb.array([
      this.fb.group(this.createItemFormGroup('new'))
    ])
  })
  
  // Get form preferences <Array> from FormArray
  public arr = this.formPrefs.get('datasets') as FormArray;

  // Using FormBuilder and also HelperService to this file
  constructor(
    private fb: FormBuilder, 
    private helperService: HelperService,
    private cashinService: CashinService,
    private router: Router
  ) { }

  ngOnInit() {     
    console.log(this.arr)
  }

  onAddRow() {
    this.arr.push(this.fb.group(this.createItemFormGroup('add')));
  }

  onRemoveRow(i: number){
    this.arr.controls.splice(i, 1);
  }

  storeIncomes() {
    let result = this.arr.controls,
    form: JQuery<HTMLElement> = $('form');
    
    if(!form.hasClass('loading'))
      form.addClass('loading');

    this.cashinService.setIncomes(result).subscribe({
      complete: () => {
        console.log('StoreIncomes Request Complete!');

        if(form.hasClass('loading'))
          form.removeClass('loading')

        console.log( this.router.navigate(['/incomes']))
      }
    })
  }

  /**
   * 
   * @param state 
   */
  createItemFormGroup(state: string) {
    let id;

    if(state == 'new') 
      id = 0
    else if(state == 'add')
      id = this.arr.controls.length

    return {
      dp: [this.helperService.getToday(), [Validators.required]],
      dpReformatted: [''],
      control: [''],
      incomeValue: ['', [Validators.required]],
      incomeDescription: ['', [Validators.required]],
      id
    }
  }

}
