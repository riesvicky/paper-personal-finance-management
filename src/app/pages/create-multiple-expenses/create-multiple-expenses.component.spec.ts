import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateMultipleExpensesComponent } from './create-multiple-expenses.component';

describe('CreateMultipleExpensesComponent', () => {
  let component: CreateMultipleExpensesComponent;
  let fixture: ComponentFixture<CreateMultipleExpensesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateMultipleExpensesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateMultipleExpensesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
