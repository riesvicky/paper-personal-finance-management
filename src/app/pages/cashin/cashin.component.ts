import { Store } from "@ngrx/store";
import { App } from "../../models/app.model";
import { AppState } from "../../app.state";
import * as AppActions from "../../actions/app.action";
import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { CashinService } from '../../services/cashin.service';
import { Observable, Subscription, fromEvent } from 'rxjs';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter
} from "rxjs/operators";
import { HelperService } from '../../services/helper.service';
import { FinanceCashinComponent, FormPref } from '../../components/modals/finance-cashin/finance-cashin.component';
import { Alert } from '../../models/alert.model';

@Component({
  selector: 'app-cashin',
  templateUrl: './cashin.component.html',
  styleUrls: ['./cashin.component.scss'],
})

export class CashinComponent implements OnInit {
  @ViewChild('searchElement', { static: true }) searchElement: ElementRef;
  @ViewChild('financemodal') child: FinanceCashinComponent;

  public title: string;
  public filter = {
    search: '',
    startDate: this.helperService.getFirstDate(),
    endDate: this.helperService.getLastDate()
  };
  public doodle_object: any[] = [
    {
      title: "Belum ada data yang ditampilkan", 
      subTitle: "Buat invoice pertama anda dan terima pembayaran masuk secepatnya!"
    },
    {
      title: "Tidak ada dokumen yang cocok dengan hasil pencarian Anda",
      subTitle: "Coba cek tanda baca atau gunakan kata kunci lainnya."
    }
  ]
  public state: LocalState = {
    fetching: false,
    dataset: JSON.parse(localStorage.getItem('APP_IN')) ? JSON.parse(localStorage.getItem('APP_IN')) : [],
    nullData: false,
    alert: {
      success: false,
      successMessage: '',
      error: false,
      errorMessage: ''
    }
  }

  constructor(
    private cashin: CashinService,
    private store: Store<AppState>, 
    private helperService: HelperService
  ) {
      this.store.dispatch(new AppActions.SetTitle('ABOUT'))
  }

  ngOnInit(): void {
    fromEvent(this.searchElement.nativeElement, 'keyup').pipe(
      // get value
      map((event: any) => {
        return event.target.value;
      })
      // if character length greater then 2
      , filter(res => res.length >= 0)
      // Time in milliseconds between key events
      , debounceTime(1000)
      // If previous query is diffent from current   
      , distinctUntilChanged()
      // subscription for response
    ).subscribe((text: string) => {    
      this.callFilter()
    });
  }
  
  callFilter() {
    this.state.fetching = true;
    console.log(this.filter)
    let _es$: Subscription = this.cashin.getIncomes(this.filter).subscribe({
      next: result => {
        if(this.filter.search != '') {
          if(result.length == 0) {
            this.state.nullData = true;
          }
        } else 
          this.state.nullData = false
        this.state.dataset = result;

        if(this.state.dataset.length > 0) {
          
          let filtered$: Subscription = this.cashin.getIncomesByDate(this.filter, this.state.dataset).subscribe({
            next: result => {
              this.state.dataset = result;

              if(this.state.dataset.length == 0)
                this.state.nullData = true
            }, 
            error: err => {
              console.error('error' + err)
            }, 
            complete: () => {
              console.log('Complete from filter')
              this.state.fetching = false;

              // Prevents memory leaking all the way through
              filtered$.unsubscribe()
            }
          });
        } else {
          this.state.fetching = false;
        } 
      }, 
      complete: () => {
        console.log('Complete from search')
        // Prevents memory leaking all the way through
        _es$.unsubscribe()
      }
    });
  }

  ngAfterViewInit() { }

  setDefaultState() {
    this.state = {
      fetching: false,
      dataset: JSON.parse(localStorage.getItem('APP_IN')) ? JSON.parse(localStorage.getItem('APP_IN')) : [],
      nullData: false,
      alert: {
        success: false,
        successMessage: '',
        error: false,
        errorMessage: ''
      }
    }
  }

  openModal(): void {
    this.child.onOpenModal();
  }

  openEditModal(dataset: FormPref) {
    this.child.onOpenEditModal(dataset);
  }

  deleteModal(dataset: FormPref): void {
    let deleteConfirm = confirm('Are you sure about this decision to delete this row?');

    if(deleteConfirm) {
      this.state.fetching = true;
      this.cashin.deleteById(dataset).subscribe({
        next: result => {
          this.setDefaultState();
        },
        complete: () => {
          this.state.fetching = false;

          this.state.alert = {
            success: false,
            successMessage: '',
            error: true,
            errorMessage: 'Income has been deleted'
          }
        }
      });
    }
  }
}

export interface LocalState {
  fetching: boolean,
  dataset: any[],
  nullData: boolean,
  alert: Alert
}


