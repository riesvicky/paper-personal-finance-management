import { Component, OnInit } from '@angular/core';
import { CONFIG } from '../../constants/config';

@Component({
  selector: 'app-knowledge-base',
  templateUrl: './knowledge-base.component.html',
  styleUrls: ['./knowledge-base.component.scss']
})
export class KnowledgeBaseComponent implements OnInit {
  public APP_URL = CONFIG.APP_URL;
  public images = {
    accountingModal     : `${this.APP_URL}/assets/images/accounting/passiva.svg`,
    iconPelajari        : `${this.APP_URL}/assets/images/main-dashboard/Icon-Pelajari.svg`,
    costPlane           : `${this.APP_URL}/assets/images/accounting/cost-plane.svg`,
    incomePlane         : `${this.APP_URL}/assets/images/accounting/income-plane.svg`,
    mobileSalesOrder    : `${this.APP_URL}/assets/images/main-dashboard/mobile-sales-order.svg`,
    mobileSalesInvoice  : `${this.APP_URL}/assets/images/main-dashboard/mobile-sales-invoice.svg`,
    mobilePaymentIn     : `${this.APP_URL}/assets/images/main-dashboard/mobile-payment-in.svg`,
    pohonDanHangar      : `${this.APP_URL}/assets/images/main-dashboard/Pohon-dan-Hangar.svg`
  }
  constructor() { }

  ngOnInit(): void {
  }

}
