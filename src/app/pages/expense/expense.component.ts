import { Component, OnInit, ViewChild, ElementRef, ChangeDetectorRef } from '@angular/core';
import { ExpenseService } from '../../services/expense.service';
import { Subscription, fromEvent } from 'rxjs';
import {
  debounceTime,
  map,
  distinctUntilChanged,
  filter
} from "rxjs/operators";
import { FinanceExpenseComponent, FormPref } from '../../components/modals/finance-expense/finance-expense.component';
import { HelperService } from '../../services/helper.service';
import { Alert } from '../../models/alert.model';

@Component({
  selector: 'app-expense',
  templateUrl: './expense.component.html',
  styleUrls: ['./expense.component.scss'],
})

export class ExpenseComponent implements OnInit {
  @ViewChild('searchElement', { static: true }) searchElement: ElementRef;
  @ViewChild('financemodal') child: FinanceExpenseComponent;

  public title: string;
  public filter = {
    search: '',
    startDate: this.helperService.getFirstDate(),
    endDate: this.helperService.getLastDate()
  };
  public doodle_object: any[] = [
    {
      title: "Belum ada data yang ditampilkan", 
      subTitle: "Buat invoice pertama anda dan terima pembayaran masuk secepatnya!"
    },
    {
      title: "Tidak ada dokumen yang cocok dengan hasil pencarian Anda",
      subTitle: "Coba cek tanda baca atau gunakan kata kunci lainnya."
    }
  ]
  public state: LocalState = {
    fetching: false,
    dataset: JSON.parse(localStorage.getItem('APP_EX')) ? JSON.parse(localStorage.getItem('APP_EX')) : [],
    nullData: false,
    alert: {
      success: false,
      successMessage: '',
      error: false,
      errorMessage: ''
    }
  }

  constructor(private expense: ExpenseService, private helperService: HelperService) {
    console.log(this.filter.endDate)
  }

  ngOnInit(): void {
    fromEvent(this.searchElement.nativeElement, 'keyup').pipe(
      // get value
      map((event: any) => {
        return event.target.value;
      })
      // if character length greater then 2
      , filter(res => res.length >= 0)
      // Time in milliseconds between key events
      , debounceTime(1000)
      // If previous query is diffent from current   
      , distinctUntilChanged()
      // subscription for response
    ).subscribe((text: string) => {    
      this.callFilter()
    });
  }
  
  callFilter() {
    this.state.fetching = true;
    
    let _es$: Subscription = this.expense.getExpenses(this.filter).subscribe({
      next: result => {
        if(this.filter.search != '') {
          if(result.length == 0) {
            this.state.nullData = true;
          }
        } else 
          this.state.nullData = false
        this.state.dataset = result;

        if(this.state.dataset.length > 0) {

          let filtered$: Subscription = this.expense.getExpensesByDate(this.filter, this.state.dataset).subscribe({
            next: result => {
              this.state.dataset = result;

              if(this.state.dataset.length == 0)
                this.state.nullData = true
            }, 
            error: err => {
              console.error('error' + err)
            }, 
            complete: () => {
              console.log('Complete from filter')
              this.state.fetching = false;

              // Prevents memory leaking all the way through
              filtered$.unsubscribe()
            }
          });
        } else {
          this.state.fetching = false;
        } 
      }, 
      error: err => {
        console.error('error' + err)
      }, 
      complete: () => {
        console.log('Complete from search')
        
        // Prevents memory leaking all the way through
        _es$.unsubscribe()
      }
    });
  }

  ngAfterViewInit() { }

  setDefaultState() {
    this.state = {
      fetching: false,
      dataset: JSON.parse(localStorage.getItem('APP_EX')) ? JSON.parse(localStorage.getItem('APP_EX')) : [],
      nullData: false,
      alert: {
        success: false,
        successMessage: '',
        error: false,
        errorMessage: ''
      }
    }
  }

  openModal(): void {
    this.child.onOpenModal();
  }

  openEditModal(dataset: FormPref) {
    this.child.onOpenEditModal(dataset);
  }

  deleteModal(dataset: FormPref): void {
    let deleteConfirm = confirm('Are you sure about this decision to delete this row?');

    if(deleteConfirm) {
      this.state.fetching = true;
      this.expense.deleteById(dataset).subscribe({
        next: result => {
          this.setDefaultState();
        },
        complete: () => {
          this.state.fetching = false;
          this.state.alert.error = true;
          this.state.alert.errorMessage = 'Your expense has been deleted'
        }
      });
    }
  }
}

export interface LocalState {
  fetching: boolean,
  dataset: any[],
  nullData: boolean,
  alert: Alert
}

